#! /usr/bin/python
"""
mininet environment used by rr_gym.
we build a topology
install flows
can no more traffic - be a termination state?
"""
import os
import sys
import time
from threading import Thread
import socket
import logging
import struct
from mininet.topo import Topo
from mininet.net import Mininet, CLI
from mininet.node import CPULimitedHost,OVSKernelSwitch,Host
from mininet.link import TCLink,Link
from mininet.log import setLogLevel,info
from mininet.cli import CLI
from mininet.node import RemoteController
import numpy as np
import random
from itertools import permutations
from copy import deepcopy
from collections import OrderedDict
from math import modf

class UserTopo(Topo):
    "Simple topology example."
    def __init__(self):
        "Create custom topo."
        # Initialize topology
        Topo.__init__( self )
        
        # Add hosts and switches
        h1 = self.addHost('h1',mac='00:00:00:00:00:01',ip='10.0.0.1')
        h2 = self.addHost('h2',mac='00:00:00:00:00:02',ip='10.0.0.2')
        h3 = self.addHost('h3',mac='00:00:00:00:00:03',ip='10.0.0.3')
        h4 = self.addHost('h4',mac='00:00:00:00:00:04',ip='10.0.0.4')
        
        sw1 = self.addSwitch('s1')
        sw2 = self.addSwitch('s2')
        sw3 = self.addSwitch('s3')
        sw4 = self.addSwitch('s4')
        sw5 = self.addSwitch('s5')
        sw6 = self.addSwitch('s6')
        
        #
        internal_linkopts = dict(bw=1) #1Mbit
        # Add links
        self.addLink('h1','s1',1,1,**internal_linkopts)
        self.addLink('h2','s1',1,2,**internal_linkopts)
        self.addLink('h3','s6',1,1,**internal_linkopts)
        self.addLink('h4','s6',1,2,**internal_linkopts)

        self.addLink('s1','s2',3,1,**internal_linkopts)
        self.addLink('s1','s4',4,1,**internal_linkopts)
        
        self.addLink('s2','s3',2,2,**internal_linkopts)
        self.addLink('s2','s4',3,2,**internal_linkopts)
        self.addLink('s2','s5',4,2,**internal_linkopts)
        
        self.addLink('s3','s4',3,3,**internal_linkopts)
        self.addLink('s3','s5',4,3,**internal_linkopts)
       
        self.addLink('s4','s5',4,4,**internal_linkopts)
        
        self.addLink('s6','s5',4,1,bw=3) # flow conservation rule
        self.addLink('s6','s3',3,1,bw=3)# flow conservation rule
                
      
class MininetEnv(object):
    def __init__(self): # initialise mininet topo environment
        self.net = Mininet(topo=UserTopo(),controller=lambda a:RemoteController(a,port=6653),ipBase='10.0.0.0/8',link=TCLink)
        self.net.start()
        self._switches = {} # self.net.switches is a list of switch objects, this dictionary holds names
        self._port_counter = {1:{1:[0,0],2:[0,0],3:[0,0],4:[0,0]},
                              2:{1:[0,0],2:[0,0],3:[0,0],4:[0,0]},
                              3:{1:[0,0],2:[0,0],3:[0,0],4:[0,0]},
                              4:{1:[0,0],2:[0,0],3:[0,0],4:[0,0]},
                              5:{1:[0,0],2:[0,0],3:[0,0],4:[0,0]},
                              6:{1:[0,0],2:[0,0],3:[0,0],4:[0,0]}} # to store 
        self.links = {(1,3):(2,1),(1,4):(4,1),
                      (2,1):(1,3),(2,2):(3,2),(2,3):(4,2),(2,4):(5,2),
                      (3,1):(6,3),(3,2):(2,2),(3,3):(4,3),(3,4):(5,3),
                      (4,1):(1,4),(4,2):(2,3),(4,3):(3,3),(4,4):(5,4),
                      (5,1):(6,4),(5,2):(2,4),(5,3):(3,4),(5,4):(4,4),
                      (6,3):(3,1),(6,4):(5,1)}
                      
        #mac addr table
        self._mac_addr = OrderedDict() # to access keys using index
        self._mac_addr = {'00:00:00:00:00:01':(1,1),'00:00:00:00:00:02':(1,2),'00:00:00:00:00:03':(6,1),'00:00:00:00:00:04':(6,2)} # since obs is discrete, fl tuple can be (0,0),(0,1) .. so in. hence, an obs can be now sent as an _mac_addr index
        
        self._all_paths = get_all_paths(self.links) # all physical paths in a network
        #print(self._all_paths)
        self._valid_paths = self.get_all_valid_paths() # all valid paths including last hope.
        #print(self._valid_paths)
        #print('----')
        self._flow_paths = self.get_flow_paths() # to store current used path
        print(self._flow_paths)
        self._state = [] # quantised state info
        self._lmh = [] # to identify links status as L,M,H [['L','M','H'],['L','M','H']]

        self._time = [0,0] # old time, new timer
        self._bw = 1048576#1Mbit
        self._cur_flow = (None,None)
        
        self.initialise()
        self.collect_port_stats()
        #CLI(self.net) #--> this seems to block execution of subsequent statements in main()
        #self.cleanup()

    def initialise(self):
        # update arp tables on hosts
        arps ={'h1':{'ip':'10.0.0.1','mac':'00:00:00:00:00:01'},
               'h2':{'ip':'10.0.0.2','mac':'00:00:00:00:00:02'},
               'h3':{'ip':'10.0.0.3','mac':'00:00:00:00:00:03'},
               'h4':{'ip':'10.0.0.4','mac':'00:00:00:00:00:04'}}
        
        for host in self.net.hosts:
            for hh in set(arps.keys())-set([host.name]):# all exclusing the curr host being conf
                host.cmd('arp -s '+ arps[hh]['ip'] + ' ' + arps[hh]['mac'] )
        #update switches dictionary
        for sw in self.net.switches: #sw is ovs switch object
            self._switches[sw.name] = sw # for each access with name
        
        #initialise various dictionaries, 
        n = len(self._switches.keys()) #number of switches
        self._state =  [[-1 for col in range(n)] for row in range(n)] # state info  , -1 for NE links
        self._lmh =  [['NE' for col in range(n)] for row in range(n)] # rewards , NE for nonexistent
        for s1,s2 in self.links.items():
            self._state[s1[0]-1][s2[0]-1] = 0
            self._lmh[s1[0]-1][s2[0]-1] = 'L1'

        #install default/last resort path for flows
        for flow,path in self._flow_paths.items():
            self.install_flows(flow,path,10) # install default flows to avoid failures at all costs
            
        #generate traffic from h1, h2 towards h3 and h4.
        for host in self.net.hosts:
            if host.name in ['h3','h4']:
                #host.cmd('iperf -s -u -i 1 >& server_{}.log&'.format(host.name))
                host.cmd('iperf -s -u &'.format(host.name))
            elif host.name == 'h1': #h1 --> h3
                host.cmd('iperf -c 10.0.0.3 -u -t 120 &') # generate traffic for 2 mins.
            else: #h2 --> h4
                host.cmd('iperf -c 10.0.0.4 -u -t 120 &')
                
    def get_all_valid_paths(self):
        valid_paths = {}
        src_sw,dst_sw = 1,6# all srcs are connected to sw 1 and all servers are connected to switch 6
        all_flows = list(permutations(list(self._mac_addr.keys()),2)) #full mac addresss
        for flow in all_flows:
            src_sw = self._mac_addr[flow[0]][0]
            dst_sw = self._mac_addr[flow[1]][0]
            if src_sw == dst_sw : paths = [[self._mac_addr[flow[1]]]]
            else:
                paths = format_path(src_sw,dst_sw,self._all_paths[(src_sw,dst_sw)],self.links)
                for each_path in paths:
                    each_path.append(self._mac_addr[flow[1]])
            valid_paths[tuple(flow)] = paths
        return valid_paths
        
    def get_flow_paths(self):
        flow_paths = {}
        for k in self._valid_paths:
            p_i = np.random.choice(len(self._valid_paths[k]))
            flow_paths[k]= self._valid_paths[k][p_i]
        return flow_paths


    #### installing add and del flow rules
    def install_flows(self,flow,path,pri): # based on paths
        for (dp,pr) in path:
            self.add_flow_cmd(dpid = dp,dl_src = flow[0], dl_dst = flow[1],outport = pr,priority=pri)# via s3
            
    def uninstall_flows(self,flow,path,pri):
        for (dp,pr) in path:
            self.del_flow_cmd(dpid = dp,dl_src = flow[0],dl_dst = flow[1],outport = pr,priority=pri)
        
    def add_flow_cmd(self,**kwargs): # match is a dictonary with keywords and actions is a list of actions
        sw = self._switches['s'+str(kwargs['dpid'])]
        cmd_ = 'ovs-ofctl add-flow s{} priority={},dl_src={},dl_dst={},actions=output:{}'.format(kwargs['dpid'],kwargs['priority'],kwargs['dl_src'],kwargs['dl_dst'],kwargs['outport'])
        sw.cmd(cmd_)
            
    def del_flow_cmd(self,**kwargs): 
        sw = self._switches['s'+ str(kwargs['dpid'])]
        cmd_ = 'ovs-ofctl del-flow s{} priority={},dl_src={},dl_dst={},actions=output:{}'.format(kwargs['dpid'],kwargs['priority'],kwargs['dl_src'],kwargs['dl_dst'],kwargs['outport'])
        sw.cmd(cmd_)
        
    def change_path(self,flow,new_path):
        old_path = self._flow_paths[flow]
        if new_path != old_path:#we are deleting and then adding as we dont want the newly added flows to be deleted, instead of old flows.
            # remove flow from old_path
            print("changing path for flow",str(flow) ,str(self._flow_paths[flow]),str(new_path))
            self.uninstall_flows(flow,old_path,20) # we are trying to remove only the high priority flows and leaving default flows as is
            # add flows to new_path
            self.install_flows(flow,new_path,20)
            # update self._flow_path to reflect new path
            self._flow_paths[flow]=new_path
        else:
            print("new path is same as old path")
      
    #### monitor links
        
    def port_stats_cmd(self,dpid):
        #print(dpid)
        sw = self._switches['s'+str(dpid)]
        stats = sw.cmd('ovs-ofctl dump-ports {}'.format(sw)).replace('\r\n',',').split(',')[1:]# omitting the first item which is some text
        #the stats are collected as strings. Hence, string manipulation has been used to extract relevant details.
        p_stats = [stats[x:x+12] for x in range(0,len(stats),12)]
        #print(p_stats)
        for i in range(1,len(p_stats)-1):# omitting port local and an empty list at the end of the list
            port = int(p_stats[i][0][15]) # this is port number
            #print(p_stats[i],p_stats[i][8])
            tx_bytes = p_stats[i][8] # 8th item is tx_bytes
            self._port_counter[dpid][port][0] = self._port_counter[dpid][port][1] # t-1,t#just a counter, not diff
            self._port_counter[dpid][port][1] = int(tx_bytes[7:])*8 #actual tx_bytes value starts from 7th char
            #print(self._port_counter[dpid])

    def collect_port_stats(self):
        #print("in collect port stats module")
        self._time[0] = self._time[1]
        #print ("port counter -->" + str(self._port_counter))
        for sw in self._port_counter.keys():
            self.port_stats_cmd(sw)
        self._time[1] = time.time()

    def update_states(self): # to uodate self._state using port_stats
        #print(self._port_counter)
        def get_lmh(lp):
            lp = modf(lp*10)[1] # d*10 --> .97* 10 = 9.7 , modf(9.7) --> (0.6999, 9.0), modf(9.7)[1] --> 9.0
            lmh = {10:'H4',9:'H3',8:'H2',7:'H1',6:'M3',5:'M2',4:'M1',3:'L4',2:'L3',1:'L2',0:'L1'}
            return lmh[lp]
        for sw1 in self._port_counter.keys():
            for pr1 in self._port_counter[sw1].keys():
                if (sw1,pr1) not in list(self._mac_addr.values()): # that is not host ports
                    sw2 = self.links[(sw1,pr1)][0]
                    diff = (self._port_counter[sw1][pr1][1] - self._port_counter[sw1][pr1][0])/(self._time[1]-self._time[0])
                    load_frac = diff/self._bw
                    if load_frac > 1:load_frac = 1 # setting up a max value
                    self._state[sw1-1][sw2-1]=round(load_frac,3) #continous , is rounding eficient
                    self._lmh[sw1-1][sw2-1] = get_lmh(load_frac)
                    #print(sw1,pr1,self._port_counter[sw1][pr1][1],self._port_counter[sw1][pr1][0],self._time[1]-self._time[0], diff,load_frac )
                else:
                    pass

    ### methods used by gym to observe

    def get_observation(self):
        s,d = random.sample(self._mac_addr.keys(),2) #full mac addresss
        self._cur_flow = (list(self._mac_addr.keys()).index(s),list(self._mac_addr.keys()).index(d)) # indexes.
        self.collect_port_stats() # 
        self.update_states()
        state = np.ravel(self._state) # unravel into tuple
        state = state[state != -1] # remove all non-exitent links
        state = np.append(state,self._cur_flow) 
       
        return state

    def get_reward(self):
        # calculate reward based on the number of L,M,H links.
        cur_count = dict(zip(*np.unique(self._lmh,return_counts=True)))
        for l in ['L1','L2','L3','L4','M1','M2','M3','H1','H2','H3','H4']:
            if cur_count.get(l) == None:cur_count[l] = 0 # if key doesnt exist,create key
        
        total_rew = cur_count['H4']*-2 + cur_count['H3']*-2 + cur_count['H2'] + cur_count['H1'] + cur_count['M3']*5 + cur_count['M2']*5 + cur_count['M1']*5  + cur_count['L4']*5 + cur_count['L3']*0 + cur_count['L2']*0 + cur_count['L1']*0
        return total_rew
        
    def get_actions(self):
        valid_actions = []
        for path in self._flow_paths.values():
            if path not in valid_actions: valid_actions.append(path)
            else: pass
        return  valid_actions# to remove duplicates
        
    def get_BW(self):
        return self._bw
    
    def get_switches(self):
        return len(self._switches.keys())

    def act(self,action): # action is a path for a flow to be installed.
        flow = (list(self._mac_addr.keys())[self._cur_flow[0]],list(self._mac_addr.keys())[self._cur_flow[1]]) # mac addresses
        if action not in self._valid_paths[flow]: #if not a valid action, then impose max penalty, we check this here as we dont want to disrupt existin flows and install incorrect flow rules.
            ob = self.get_observation()
            rew = -5 # bad decision , reducing bad decision penalisation. This is because a bad decision doesnt change the network state from congested to less congested, and since we are already penalising for high usage,penatly for bad decision would be double penalisation.
            print("invalid path {} for flow {}".format(action,flow))
        else:#if correct path is chosen then reward, then check if congestion has reduced.

            print("valid path {} for flow{}".format(action,flow))
            old_path = self._flow_paths[flow]
            if old_path != action:# ignoring last hop which is the host hop
                self.change_path(flow,action)#flow = ()
                time.sleep(10)#to allow some traffic to pass to make observations
            else:
                print("new path {} is same as old path {}.nothing to re-configure".format(old_path,action))
                
        ob = self.get_observation()
        rew = self.get_reward()
        done = False
        if (ob[:20] == np.zeros(20)).all(): # if no more traffic, then end
            done = True
        return ob,rew,done # delete from old switches and install onto new switches.

    def cleanup(self):
        self.net.stop()
        #self.net.start()
        
    def reset(self): # to bring the env to earlier state.
        #self.cleanup()
        self.initialise()
        

#### DFS

def commence_dfs(s,d,topo):
    return dfs(topo,s,d,list(),list())
    
def dfs(G,cur_node,dst,visited,all_paths): # dfs using recursion
  visited.append(cur_node) # mark src as visited
  if cur_node == dst: # base condition
    all_paths.append(deepcopy(visited))## since path gets modified, the modification also seems to be affecting all_paths, hence a deepcopy
  else:
    for nei in G[cur_node]:
      if nei not in visited:
        dfs(G,nei,dst,visited,all_paths) # same variable path is being updated
  visited.pop() #visited.remove(cur_node)
  return all_paths

def get_all_paths(links):
    all_sd = list(permutations(range(1,7),2))
    paths = {}
    for (s,d) in all_sd:
        paths[(s,d)]=[]
    topo = {1:[],2:[],3:[],4:[],5:[],6:[]}
    for n1,n2 in links.items():
        topo[n1[0]].append(n2[0])
    for s,d in all_sd:
        c = commence_dfs(s,d,topo)
        paths[(s,d)]=c
    #acp = format_path(1,6,paths,links)
    return paths

def format_path(s,d,paths,links):
    completed_path = []
    all_completed_paths =[]
    for path in paths:
        for i in range(0,len(path)-1):
            for n1,n2 in links.items():
                if n1[0] == path[i] and n2[0] == path[i+1]:
                    completed_path.append(n1)
                else:
                    continue
        all_completed_paths.append(completed_path)
        completed_path = []
    return all_completed_paths
        
if __name__ == '__main__':
    #setLogLevel('error')
    me = MininetEnv()
    print("!!!!!!!")
    time.sleep(3)
    #print("_switches-->{}".format(me._switches))
    #
    #print("links-->{}".format(me.links))
    print("_all_paths-->{}".format(me._all_paths))
    #print("sd-->{}".format(me.sd))
    print("_valid_paths1-->{}".format(me._valid_paths))
    print("flow_paths-->{}".format(me._flow_paths))
    #print("state-->{}".format(me._state))
    #print("lmh-->{}".format(me._lmh))
    #print("_time-->{}".format(me._time))
    #print("bw-->{}".format(me._bw))
    #print("cur_flow-->{}".format(me._cur_flow))
    i = 0
    while i < 2:
        print("_port_counter-->{}".format(me._port_counter))
        print ("unraveled",me.get_observation())
        print("-------")
        print("state-->{}".format(me._state))
        print("lmh-->{}".format(me._lmh))
        print ("reward",me.get_reward())
        i += 1
    
    #print("_valid_paths2-->{}".format(me._valid_paths))
    print("actions",me.get_actions())


