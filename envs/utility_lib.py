#Utility lib
from itertools import permutations,groupby
from copy import deepcopy

#install arp table entries
def update_arp(hosts):
    ip_mac = {}
    for i in range(1,len(hosts)+1):
        ip_mac['h'+str(i)]={'ip':'10.0.0.'+str(i),'mac':'00:00:00:00:00:0'+str(i)}
        
    for host in hosts:
        for hh in set(ip_mac.keys())-set([host.name]):# all exclusing the curr host being conf
            host.cmd('arp -s '+ ip_mac[hh]['ip'] + ' ' + ip_mac[hh]['mac'])
            
            
# path functions            
def get_all_paths(links):
    all_sd = list(permutations(range(1,6),2))
    paths = {}
    for (s,d) in all_sd:
        paths[(s,d)]=[]
    topo = {1:[],2:[],3:[],4:[],5:[]}
    for n1,n2 in links.items():
        topo[n1[0]].append(n2[0])
    for s,d in all_sd:
        c = commence_dfs(s,d,topo)
        paths[(s,d)]=c
    return paths

def commence_dfs(s,d,topo):
    return dfs(topo,s,d,list(),list())
    
def dfs(G,cur_node,dst,visited,all_paths): # dfs using recursion
  visited.append(cur_node) # mark src as visited
  if cur_node == dst: # base condition
    all_paths.append(deepcopy(visited))## since path gets modified, the modification also seems to be affecting all_paths, hence a deepcopy
  else:
    for nei in G[cur_node]:
      if nei not in visited:
        dfs(G,nei,dst,visited,all_paths) # same variable path is being updated
  visited.pop() #visited.remove(cur_node)
  return all_paths
  
def format_path(s,d,paths,links):
    completed_path = []
    all_completed_paths =[]
    for path in paths:
        for i in range(0,len(path)-1):
            for n1,n2 in links.items():
                if n1[0] == path[i] and n2[0] == path[i+1]:
                    completed_path.append(n1)
                else:
                    continue
        if len(complete_path) > 4 : #ignore longest loopy paths
        	all_completed_paths.append(completed_path)
        completed_path = []
    return all_completed_paths
    

##### SPF #####
def sort_dist(dist):
    dist_list=[]
    for k in sorted(dist,key=dist.get):
        dist_list.append([k,dist[k]])
    return dist_list

def frame_path(parent,src,dst):
    path=[dst]
    while dst != src:
        p = parent[dst]
        path.append(p)
        dst=p
    path.reverse()
    return path

def shortestpath(G,src,dst):
    visited = [src]
    dist = {}
    parents = {src:src} # update this to track last good node using that lead us here
    for node in G.keys():
        if node in G[src].keys():
            dist[node]=G[src][node]
            parents[node]=src
        else:
            dist[node] = 1000000 # some random high value
    dist[src]=0
    dl = sort_dist(dist) # this will arrange the dictionary from lowest to highest values
    while len(visited) < len(G):
        node = dl.pop(0) # lowest cost node
        if node[0] not in visited:
            visited.append(node[0])
            for nei in G[node[0]].keys():
                if nei not in visited:
                    dist[nei] = min(dist[nei] , dist[node[0]]+G[node[0]][nei])
                    if dist[nei] == dist[node[0]]+G[node[0]][nei]:
                        parents[nei] = node[0]
                    dl = sort_dist(dist)
    path = frame_path(parents,src,dst)
    return path


    
