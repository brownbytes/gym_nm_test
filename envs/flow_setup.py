### flowtable management

def add_flow_cmd(**kwargs): # match is a dictonary with keywords and actions is a list of actions
    cmd_ = 'ovs-ofctl add-flow s{} priority={},dl_src={},dl_dst={},actions=output:{}'.format(kwargs['dpid'],kwargs['priority'],kwargs['dl_src'],kwargs['dl_dst'],kwargs['outport'])
    return cmd_
    
def del_flow_cmd(**kwargs): 
    cmd_ = 'ovs-ofctl del-flow s{} priority={},dl_src={},dl_dst={},actions=output:{}'.format(kwargs['dpid'],kwargs['priority'],kwargs['dl_src'],kwargs['dl_dst'],kwargs['outport'])
    return cmd_

##
def port_stats_cmd(sw):
    sw_pr ={}
    stats = sw.cmd('ovs-ofctl dump-ports {}'.format(sw)).replace('\r\n',',').split(',')[1:]
    p_stats = [stats[x:x+12] for x in range(0,len(stats),12)]
    for i in range(1,len(p_stats)-1):
        port = int(p_stats[i][0][15]) # this is port number
        tx_bytes = p_stats[i][8] # 8th item is tx_bytes
        sw_pr[port]=int(tx_bytes[7:])*8
    #print(sw_pr)
    return sw_pr
    
def flow_stats_cmd(sw,flow,pr):
    flow_stats = sw.cmd('ovs-ofctl dump-flows {} dl_src={},dl_dst={},out_port={}'.format(sw,flow[0],flow[1],pr)).split('\r\n')[:-1]
    for flow_stat in flow_stats:
        if flow_stat != '' and int(flow_stat.split(',')[5].split("=")[1])==20: # new flow
            duration = float(flow_stat.split(',')[1].split("=")[1][:-1])
            n_bytes = int(flow_stat.split(',')[4].split("=")[1])
        elif flow_stat != '' and int(flow_stat.split(',')[5].split("=")[1])==10: # default flow
            duration = float(flow_stat.split(',')[1].split("=")[1][:-1])
            n_bytes = int(flow_stat.split(',')[4].split("=")[1])
    return duration,n_bytes
        
##configure queues

def config_queue(dpid,pr):#should be done after finding a path,sw obj also while changing flows
    queues_cmd = "ovs-vsctl -- set port s1-eth{} qos=@newqos -- --id=@newqos create qos type=linux-htb queues=0=@q0,1=@q1 -- --id=@q0 create queue other-config:min-rate=0 other-config:max-rate=300000 -- --id=@q1 create queue other-config:min-rate=0 other-config:max-rate=700000".format(dpid,pr)

def add_q_flow(**kwargs): #switch object
    cmd_ = "ovs-ofctl add-flow s{} dl_src={},dl_dst={},priority=20,actions=enqueue:{}:1".format(kwargs['dpid'],kwargs['dl_src'],kwargs['dl_dst'],kwargs['outport'])
    return cmd_
