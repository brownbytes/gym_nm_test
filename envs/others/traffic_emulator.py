#emulate network traffic patterns
import numpy as np
import random
import time

class Traffic_Em():
    
    def __init__(self):
        self.switches = [1,2,3,4]
        self.hosts = [1,2,3,4]
        self.valid_paths = {(1,2):[[1,2]],
               (1,3):[[[1,3],[3,2],[2,1]],[[1,4],[4,2],[2,1]]],
               (1,4):[[[1,3],[3,2],[2,2]],[[1,4],[4,2],[2,2]]],
               (2,1):[[1,1]],
               (2,3):[[[1,3],[3,2],[2,1]],[[1,4],[4,2],[2,1]]],
               (2,4):[[[1,3],[3,2],[2,2]],[[1,4],[4,2],[2,2]]],
               (3,1):[[[2,3],[3,1],[1,1]],[[2,4],[4,1],[1,1]]],
               (3,2):[[[2,3],[3,1],[1,2]],[[2,4],[4,1],[1,2]]],
               (3,4):[[2,2]],
               (4,1):[[[2,3],[3,1],[1,1]],[[2,4],[4,1],[1,1]]],
               (4,2):[[[2,3],[3,1],[1,2]],[[2,4],[4,1],[1,2]]],
               (4,3):[[2,1]]
               }# (s,d):[sw,pr]
        self.bw = 1048576
        self.port_counter = {1:{1:[0,0],2:[0,0],3:[0,0],4:[0,0]},2:{1:[0,0],2:[0,0],3:[0,0],4:[0,0]},3:{1:[0,0],2:[0,0]},4:{1:[0,0],2:[0,0]}} # to store
        self.links = {(1,3):(3,1),(1,4):(4,1),(2,3):(3,2),(2,4):(4,2),(3,1):(1,3),(3,2):(2,3),(4,1):(1,4),(4,2):(2,4)}
        self.active_flows = [(1,3),(2,4)] # (h1,h3),(h2,h4)
        self.ticks = 0
        self.reset()
        self.cur_flow = (None,None)
        
    def reset(self):
        self.flow_paths = {(1,2):[[1,2]],
                      (1,3):[[1,3],[3,2],[2,1]],
                      (1,4):[[1,3],[3,2],[2,2]],
                      (2,1):[[1,1]],
                      (2,3):[[1,3],[3,2],[2,1]],
                      (2,4):[[1,3],[3,2],[2,2]],
                      (3,1):[[2,3],[3,1],[1,1]],
                      (3,2):[[2,3],[3,1],[1,2]],
                      (3,4):[[2,2]],
                      (4,1):[[2,3],[3,1],[1,1]],
                      (4,2):[[2,3],[3,1],[1,2]],
                      (4,3):[[2,1]]
                      }#(s,d):[sw,pr]
        self.state = [[-1,-1,0,0],[-1,-1,0,0],[0,0,-1,-1],[0,0,-1,-1]]# -1 for non-existent connections
        self.lmh = [['NE','NE','L1','L1'],['NE','NE','L1','L1'],['L1','L1','NE','NE'],['L1','L1','NE','NE']]# to calculate reward
        self.timer = [0,0.0001] # old time, new time
        self.ticks = False
        
    def gen_sd(self):# generate random flow
        flow = random.sample(self.hosts,2)
        return flow

    def update_states(self): # update states and lmh
        for sw1 in self.port_counter.keys():
            for pr1 in self.port_counter[sw1].keys():
                if (sw1,pr1) not in [(1,1),(1,2),(2,1),(2,2)]: # that is not host ports
                    sw2 = self.links[(sw1,pr1)][0]
                    diff = (self.port_counter[sw1][pr1][1] - self.port_counter[sw1][pr1][0])/(self.timer[1]-self.timer[0])
                    load_percentile = diff/self.bw
                    self.state[sw1-1][sw2-1]=load_percentile
                    if load_percentile >= 1:
                        self.state[sw1-1][sw2-1] = 10
                        self.lmh[sw1-1][sw2-1] = 'H4'
                    elif load_percentile >= .9:
                        self.state[sw1-1][sw2-1] = 9
                        self.lmh[sw1-1][sw2-1] = 'H3'
                    elif load_percentile >= .8:
                        self.state[sw1-1][sw2-1] = 8
                        self.lmh[sw1-1][sw2-1] = 'H2'
                    elif load_percentile >= .7:
                        self.state[sw1-1][sw2-1] = 7
                        self.lmh[sw1-1][sw2-1] = 'H1'
                    elif load_percentile >= .6:
                        self.state[sw1-1][sw2-1] = 6
                        self.lmh[sw1-1][sw2-1] = 'M3'
                    elif load_percentile >= .5:
                        self.state[sw1-1][sw2-1] = 5
                        self.lmh[sw1-1][sw2-1] = 'M2'
                    elif load_percentile >= .4:
                        self.state[sw1-1][sw2-1] = 4
                        self.lmh[sw1-1][sw2-1] = 'M1'
                    elif load_percentile >= .3:
                        self.state[sw1-1][sw2-1] = 3
                        self.lmh[sw1-1][sw2-1] = 'L4'
                    elif load_percentile >= .2:
                        self.state[sw1-1][sw2-1] = 2
                        self.lmh[sw1-1][sw2-1] = 'L3'
                    elif load_percentile >= .1:
                        self.state[sw1-1][sw2-1] = 1
                        self.lmh[sw1-1][sw2-1] = 'L2'
                    elif load_percentile >= 0:
                        self.state[sw1-1][sw2-1] = 0 # -1 for non-existent links
                        self.lmh[sw1-1][sw2-1] = 'L1'
                    #print((sw1,pr1),self.port_counter[sw1][pr1][1],self.port_counter[sw1][pr1][0],self.timer[1]-self.timer[0],diff,load_percentile)


    def get_observation(self):
        self.gen_traffic(self.active_flows) # 
        self.update_states()
        self.timer[0] = self.timer[1]
        self.timer[1] = time.time()
        self.cur_flow = tuple(self.gen_sd()) # random s and d
        state = np.ravel(self.state) # unravel into tuple
        state = state[state != -1] # remove all non-exitent links
        state = np.append(state,self.cur_flow)
        return state

    def get_reward(self):
        # calculate reward based on the number of L,M,H links.
        cur_count = dict(zip(*np.unique(self.lmh,return_counts=True)))
        for l in ['L1','L2','L3','L4','M1','M2','M3','H1','H2','H3','H4']:
            if cur_count.get(l) == None:cur_count[l] = 0 # if key doesnt exist,create key
        total_rew = cur_count['H4']*0 + cur_count['H3']*0 + cur_count['H2']*0 + cur_count['H1']*1 + cur_count['M3']*5 + cur_count['M2']*10 + cur_count['M1']*10  + cur_count['L4']*2 + cur_count['L3'] + cur_count['L2'] + cur_count['L1'] 
        return total_rew
        

    def get_switches(self):
        return self.switches
    
    def get_all_actions(self):
        return  [[[1,3],[3,2],[2,1]],[[1,4],[4,2],[2,1]],[[1,3],[3,2],[2,2]],[[1,4],[4,2],[2,2]],[[2,3],[3,1],[1,1]],[[2,4],[4,1],[1,1]],[[2,3],[3,1],[1,2]],[[2,4],[4,1],[1,2]]] # assuming the RL agent 

    def gen_traffic(self,flows): #emulate traffic generation and update port_counter.
        # get path between source and destination
        # generate a random number based on some distribution between source and destination
        # add this number to the ports of switches in the path
        for (s,d) in flows:
            path = self.flow_paths[(s,d)]
            traffic = np.random.uniform(0.0,self.bw/len(flows)) # include a distribution here, 2 flows (h1,h3),(h2,h4). This is bandaid to avoid over provisioning the link
            for (sw,pr) in path:
                self.port_counter[sw][pr][0] = self.port_counter[sw][pr][1] # t-1,t#just a counter, not diff
                self.port_counter[sw][pr][1] += traffic #actual tx_bytes value starts from 7th char

    def episode_done(self):
        if self.ticks > 250: # 120 values in each episode
            return True
        else:
            return False
        
    def act(self,action): # action is a path for a flow to be installed.
        if action not in self.valid_paths[self.cur_flow]: #if not a valid action, then impose max penalty, we check this here as we dont want to disrupt existin flows and install incorrect flow rules.
            ob = self.get_observation()
            rew = -5 # bad decision
            print("bad decisison")
        else:#if correct path is chosen then reward, then check if congestion has reduced.
            print("correct path.")
            self.flow_paths[self.cur_flow]=action #flow = ()
            ob = self.get_observation()
            rew = self.get_reward()
        done = False
        self.ticks += 1
        if self.episode_done(): # if no more traffic, then end
            done = True
        return ob,rew,done # delete from old switches and install onto new switches.

if __name__ =='__main__':
    te=Traffic_Em()
    te.timer[1]=time.time()
    te.cur_flow = (1,3)
    te.act([[1,4],[4,2],[2,1]])
    print(te.flow_paths)
    
    

