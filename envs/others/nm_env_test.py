#!/usr/bin/env python

import gym
from gym import error,spaces,utils
from gym.utils import seeding
from gym import spaces

import numpy as np
import datetime
import random
from sortedcontainers import SortedDict as sd
import sys
from copy import deepcopy
import pickle
import csv
import logging.config
import math
import requests

#from gym_nm_test.envs.traffic_emulator import Traffic_Em
from gym_nm_test.envs.mininet_be_test_v1 import MininetEnv

class NMEnvTest(gym.Env): # NM enviornment
    def __init__(self): # duration of an episode before a reward is reaped
    
        self.__version__ = "0.1.0"
        logging.info("NMEnv - version {}".format(self.__version__))
        #self.te = Traffic_Em()
        self.te = MininetEnv()
        # # define action space # same as number of paths
        self.actions = self.te.get_all_actions()# action : all actions
        self.action_space = spaces.Discrete(len(self.actions)) 
        low = np.append(np.zeros([8]),np.array([0,0],dtype=np.int8))#np.zeros[16]
        high = np.append(np.array([11]*8),np.array([3,3],dtype=np.int8))#np.array[9]*16#max value of 10
        n_switches = self.te.get_switches()# number of switches in the network
        self.observation_space = spaces.Box(low,high) 
        self.__episode_over=False
        
    def step(self,action):# take some action in this step
        ob,reward,self.__episode_over = self.te.act(self.actions[action])
        return ob,reward,self.__episode_over,{}
        
    def get_state(self):
        ob = self.te.get_observation()# ravel and append in mininet
        return ob
    
    def reset(self): # reset the state of the environment and return an intial observation
        self.__episode_over = False
        self.te.reset()
        reward = 0
        return self.get_state()

    def cleanup(self):
        self.te.cleanup()

