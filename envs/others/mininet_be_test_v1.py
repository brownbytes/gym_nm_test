#! /usr/bin/python
"""
mininet environment used by rr_gym.
we build a topology
install flows
can no more traffic - be a termination state?
"""
import os
import sys
import time
from threading import Thread
import socket
import logging
import struct
from mininet.topo import Topo
from mininet.net import Mininet, CLI
from mininet.node import CPULimitedHost,OVSKernelSwitch,Host
from mininet.link import TCLink,Link
from mininet.log import setLogLevel,info
from mininet.cli import CLI
from mininet.node import RemoteController
import numpy as np
import random

class UserTopo(Topo):
    "Simple topology example."
    def __init__(self):
        "Create custom topo."
        # Initialize topology
        Topo.__init__( self )
        
        # Add hosts and switches
        h1 = self.addHost('h1',mac='00:00:00:00:00:01',ip='10.0.0.1')
        h2 = self.addHost('h2',mac='00:00:00:00:00:02',ip='10.0.0.2')
        h3 = self.addHost('h3',mac='00:00:00:00:00:03',ip='10.0.0.3')
        h4 = self.addHost('h4',mac='00:00:00:00:00:04',ip='10.0.0.4')
        sw1 = self.addSwitch('s1')
        sw2 = self.addSwitch('s2')
        sw3 = self.addSwitch('s3')
        sw4 = self.addSwitch('s4')
        #
        linkopts = dict(bw=1) #1Mbit
        # Add links
        self.addLink('h1','s1',1,1,**linkopts)
        self.addLink('h2','s1',1,2,**linkopts)
        self.addLink('h3','s2',1,1,**linkopts)
        self.addLink('h4','s2',1,2,**linkopts)

        self.addLink('s1','s3',3,1,**linkopts)
        self.addLink('s1','s4',4,1,**linkopts)
        self.addLink('s2','s3',3,2,**linkopts)
        self.addLink('s2','s4',4,2,**linkopts)
        
      
class MininetEnv(object):
    def __init__(self): # initialise mininet topo environment
        self.net = Mininet(topo=UserTopo(),controller=lambda a:RemoteController(a,port=6653),ipBase='10.0.0.0/8',link=TCLink)
        self.net.start()
        self.__switches = {}
        self.__port_counter = {1:{1:[0,0],2:[0,0],3:[0,0],4:[0,0]},2:{1:[0,0],2:[0,0],3:[0,0],4:[0,0]},3:{1:[0,0],2:[0,0]},4:{1:[0,0],2:[0,0]}} # to store 
        self.__all_paths = {('00:00:00:00:00:01','00:00:00:00:00:02'):[[1,2]],
                            ('00:00:00:00:00:01','00:00:00:00:00:03'):[[[1,3],[3,2],[2,1]],[[1,4],[4,2],[2,1]]],
                            ('00:00:00:00:00:01','00:00:00:00:00:04'):[[[1,3],[3,2],[2,2]],[[1,4],[4,2],[2,2]]],
                            ('00:00:00:00:00:02','00:00:00:00:00:01'):[[1,1]],
                            ('00:00:00:00:00:02','00:00:00:00:00:03'):[[[1,3],[3,2],[2,1]],[[1,4],[4,2],[2,1]]],
                            ('00:00:00:00:00:02','00:00:00:00:00:04'):[[[1,3],[3,2],[2,2]],[[1,4],[4,2],[2,2]]],
                            ('00:00:00:00:00:03','00:00:00:00:00:01'):[[[2,3],[3,1],[1,1]],[[2,4],[4,1],[1,1]]],
                            ('00:00:00:00:00:03','00:00:00:00:00:02'):[[[2,3],[3,1],[1,2]],[[2,4],[4,1],[1,2]]],
                            ('00:00:00:00:00:03','00:00:00:00:00:04'):[[2,2]],
                            ('00:00:00:00:00:04','00:00:00:00:00:01'):[[[2,3],[3,1],[1,1]],[[2,4],[4,1],[1,1]]],
                            ('00:00:00:00:00:04','00:00:00:00:00:02'):[[[2,3],[3,1],[1,2]],[[2,4],[4,1],[1,2]]],
                            ('00:00:00:00:00:04','00:00:00:00:00:03'):[[2,1]]
                            }# [sw1,outport]
        
        self.__flow_paths = dict.fromkeys(self.__all_paths.keys()) #path info to avoid provisioning if old path is same as new path
        self.__state = [] # quantised state info
        self.__lmh = [] # to identify links status as L,M,H [['L','M','H'],['L','M','H']]
        self.__links = {(1,3):(3,1),(1,4):(4,1),(2,3):(3,2),(2,4):(4,2),(3,1):(1,3),(3,2):(2,3),(4,1):(1,4),(4,2):(2,4)}
        self.__time = [0,0] # old time, new time
        self.__bw = 1048576#1Mbit
        self._cur_flow = (None,None)
        
        self.initialise()
        self.collect_port_stats()
        #CLI(self.net) #--> this seems to block execution of subsequent statements in main()
        #self.cleanup()

    def initialise(self):
        # update arp tables on hosts
        arps ={'h1':{'ip':'10.0.0.1','mac':'00:00:00:00:00:01'},
               'h2':{'ip':'10.0.0.2','mac':'00:00:00:00:00:02'},
               'h3':{'ip':'10.0.0.3','mac':'00:00:00:00:00:03'},
               'h4':{'ip':'10.0.0.4','mac':'00:00:00:00:00:04'}}
        
        for host in self.net.hosts:
            for hh in set(arps.keys())-set([host.name]):# all exclusing the curr host being conf
                host.cmd('arp -s '+ arps[hh]['ip'] + ' ' + arps[hh]['mac'] )
        #update switches dictionary
        for sw in self.net.switches: #sw is ovs switch object
            self.__switches[sw.name] = sw # for each access with name

        #initialise various dictionaries, this needs to be of size 8 due to non-existent links.
        n = len(self.__switches.keys()) #number of switches
        self.__state = [[-1,-1,0,0],[-1,-1,0,0],[0,0,-1,-1],[0,0,-1,-1]]# -1 for non-existent connections
        self.__lmh = [['NE','NE','L1','L1'],['NE','NE','L1','L1'],['L1','L1','NE','NE'],['L1','L1','NE','NE']]# to calculate reward

        #install default/last resort path for flows
        self.__flow_paths = {('00:00:00:00:00:01','00:00:00:00:00:02'):[[1,2]],
                             ('00:00:00:00:00:01','00:00:00:00:00:03'):[[1,3],[3,2],[2,1]],
                             ('00:00:00:00:00:01','00:00:00:00:00:04'):[[1,3],[3,2],[2,2]],
                             ('00:00:00:00:00:02','00:00:00:00:00:01'):[[1,1]],
                             ('00:00:00:00:00:02','00:00:00:00:00:03'):[[1,3],[3,2],[2,1]],
                             ('00:00:00:00:00:02','00:00:00:00:00:04'):[[1,3],[3,2],[2,2]],
                             ('00:00:00:00:00:03','00:00:00:00:00:01'):[[2,3],[3,1],[1,1]],
                             ('00:00:00:00:00:03','00:00:00:00:00:02'):[[2,3],[3,1],[1,2]],
                             ('00:00:00:00:00:03','00:00:00:00:00:04'):[[2,2]],
                             ('00:00:00:00:00:04','00:00:00:00:00:01'):[[2,3],[3,1],[1,1]],
                             ('00:00:00:00:00:04','00:00:00:00:00:02'):[[2,3],[3,1],[1,2]],
                             ('00:00:00:00:00:04','00:00:00:00:00:03'):[[2,1]]
                             }
        
        self.__mac_addr = ['00:00:00:00:00:01','00:00:00:00:00:02','00:00:00:00:00:03','00:00:00:00:00:04'] # since obs is discrete, fl tuple can be (0,0),(0,1) .. so in. hence, an obs can be now sent as an __mac_addr index

        for flow,path in self.__flow_paths.items():
            self.install_flows(flow,path,10) # install default flows to avoid failures at all costs
            
        #generate traffic from h1, h2 towards h3 and h4.
        for host in self.net.hosts:
            if host.name in ['h3','h4']:
                host.cmd('iperf -s -u -i 1 >& server_{}.log&'.format(host.name) )
            elif host.name == 'h1': #h1 --> h3
                host.cmd('iperf -c 10.0.0.3 -u -t 120 &') # generate traffic for 2 mins.
            else: #h2 --> h4
                host.cmd('iperf -c 10.0.0.4 -u -t 120 & ')
                
        
    #### installing add and del flow rules
    def install_flows(self,flow,path,pri): # based on paths
        for (dp,pr) in path:
            self.add_flow_cmd(dpid = dp,dl_src = flow[0], dl_dst = flow[1],outport = pr,priority=pri)# via s3
            
    def uninstall_flows(self,flow,path,pri):
        for (dp,pr) in path:
            self.del_flow_cmd(dpid = dp,dl_src = flow[0],dl_dst = flow[1],outport = pr,priority=pri)
        
    def add_flow_cmd(self,**kwargs): # match is a dictonary with keywords and actions is a list of actions
        sw = self.__switches['s'+str(kwargs['dpid'])]
        cmd_ = 'ovs-ofctl add-flow s{} priority={},dl_src={},dl_dst={},actions=output:{}'.format(kwargs['dpid'],kwargs['priority'],kwargs['dl_src'],kwargs['dl_dst'],kwargs['outport'])
        sw.cmd(cmd_)
            
    def del_flow_cmd(self,**kwargs): 
        sw = self.__switches['s'+ str(kwargs['dpid'])]
        cmd_ = 'ovs-ofctl del-flow s{} priority={},dl_src={},dl_dst={},actions=output:{}'.format(kwargs['dpid'],kwargs['priority'],kwargs['dl_src'],kwargs['dl_dst'],kwargs['outport'])
        sw.cmd(cmd_)
        
    def change_path(self,flow,new_path,old_path):
        #old_path = self.__flow_paths[flow]
        #if new_path != old_path:#we are deleting and then adding as we dont want the newly added flows to be deleted, instead of old flows.
        # remove flow from old_path
        print("changing path for flow",str(flow) ,str(self.__flow_paths[flow]),str(new_path))
        self.uninstall_flows(flow,old_path,20) # we are trying to remove only the high priority flows and leaving default flows as is
        # add flows to new_path
        self.install_flows(flow,new_path,20)
        # update self.__flow_path to reflect new path
        self.__flow_paths[flow]=new_path
        #else:
            #print("new path is same as old path")
      
    #### monitor links
        
    def port_stats_cmd(self,dpid):
        sw = self.__switches['s'+str(dpid)]
        stats = sw.cmd('ovs-ofctl dump-ports s{}'.format(dpid)).replace('\r\n',',').split(',')[1:]# omitting the first item which is some text
        #the stats are collected as strings. Hence, string manipulation has been used to extract relevant details.
        p_stats = [stats[x:x+12] for x in range(0,len(stats),12)]
        #print(p_stats)
        for i in range(1,len(p_stats)-1):# omitting port local and an empty list at the end of the list
            port = int(p_stats[i][0][15]) # this is port number
            #print(p_stats[i],p_stats[i][8])
            tx_bytes = p_stats[i][8] # 8th item is tx_bytes
            self.__port_counter[dpid][port][0] = self.__port_counter[dpid][port][1] # t-1,t#just a counter, not diff
            self.__port_counter[dpid][port][1] = int(tx_bytes[7:])*8 #actual tx_bytes value starts from 7th char
            #print(self.__port_counter[dpid])

    def collect_port_stats(self):
        self.__time[0] = self.__time[1]
        for sw in self.__port_counter:
            self.port_stats_cmd(sw)
        #print ("port counter -->" + str(self.__port_counter))
        self.__time[1] = time.time()

    def update_states(self): # to uodate self.__state using port_stats
        #print(self.__port_counter)
        for sw1 in self.__port_counter.keys():
            for pr1 in self.__port_counter[sw1].keys():
                if (sw1,pr1) not in [(1,1),(1,2),(2,1),(2,2)]: # that is not host ports
                    sw2 = self.__links[(sw1,pr1)][0]
                    diff = (self.__port_counter[sw1][pr1][1] - self.__port_counter[sw1][pr1][0])/(self.__time[1]-self.__time[0])
                    load_percentile = diff/max(diff,self.__bw)
                    self.__state[sw1-1][sw2-1]=load_percentile #continous
                    if load_percentile >= 1:
                        #self.__state[sw1-1][sw2-1] = 10
                        self.__lmh[sw1-1][sw2-1] = 'H4'
                    elif load_percentile >= .9:
                        #self.__state[sw1-1][sw2-1] = 9
                        self.__lmh[sw1-1][sw2-1] = 'H3'
                    elif load_percentile >= .8:
                        #self.__state[sw1-1][sw2-1] = 8
                        self.__lmh[sw1-1][sw2-1] = 'H2'
                    elif load_percentile >= .7:
                        #self.__state[sw1-1][sw2-1] = 7
                        self.__lmh[sw1-1][sw2-1] = 'H1'
                    elif load_percentile >= .6:
                        #self.__state[sw1-1][sw2-1] = 6
                        self.__lmh[sw1-1][sw2-1] = 'M3'
                    elif load_percentile >= .5:
                        #self.__state[sw1-1][sw2-1] = 5
                        self.__lmh[sw1-1][sw2-1] = 'M2'
                    elif load_percentile >= .4:
                        #self.__state[sw1-1][sw2-1] = 4
                        self.__lmh[sw1-1][sw2-1] = 'M1'
                    elif load_percentile >= .3:
                        #self.__state[sw1-1][sw2-1] = 3
                        self.__lmh[sw1-1][sw2-1] = 'L4'
                    elif load_percentile >= .2:
                        #self.__state[sw1-1][sw2-1] = 2
                        self.__lmh[sw1-1][sw2-1] = 'L3'
                    elif load_percentile >= .1:
                        #self.__state[sw1-1][sw2-1] = 1
                        self.__lmh[sw1-1][sw2-1] = 'L2'
                    elif load_percentile >= 0:
                        #self.__state[sw1-1][sw2-1] = 0 # -1 for non-existent links
                        self. __lmh[sw1-1][sw2-1] = 'L1'
                    #print(sw1,pr1,self.__port_counter[sw1][pr1][1],self.__port_counter[sw1][pr1][0],self.__time[1]-self.__time[0], diff,load_percentile )
        

    ### methods used by gym to observe

    def get_observation(self):
        self.collect_port_stats() # 
        self.update_states()
        
        s,d = random.sample(self.__mac_addr,2)
        self._cur_flow = (self.__mac_addr.index(s),self.__mac_addr.index(d))
        state = np.ravel(self.__state) # unravel into tuple
        state = state[state != -1] # remove all non-exitent links
        state = np.append(state,self._cur_flow) 
        return state

    def get_reward(self):
        # calculate reward based on the number of L,M,H links.
        cur_count = dict(zip(*np.unique(self.__lmh,return_counts=True)))
        for l in ['L1','L2','L3','L4','M1','M2','M3','H1','H2','H3','H4']:
            if cur_count.get(l) == None:cur_count[l] = 0 # if key doesnt exist,create key
        
        total_rew = cur_count['H4'] + cur_count['H3'] + cur_count['H2'] + cur_count['H1'] + cur_count['M3']*5 + cur_count['M2']*10 + cur_count['M1']*10  + cur_count['L4']*5 + cur_count['L3']*0 + cur_count['L2']*0 + cur_count['L1']*0
        return total_rew
        
    def get_all_actions(self):
        return  [[[1,3],[3,2],[2,1]],[[1,4],[4,2],[2,1]],[[1,3],[3,2],[2,2]],[[1,4],[4,2],[2,2]],[[2,3],[3,1],[1,1]],[[2,4],[4,1],[1,1]],[[2,3],[3,1],[1,2]],[[2,4],[4,1],[1,2]]] # assuming the RL agent 
        
    def get_BW(self):
        return self.__bw
    
    def get_switches(self):
        return len(self.__switches.keys())

    def act(self,action): # action is a path for a flow to be installed.
        flow = (self.__mac_addr[self._cur_flow[0]],self.__mac_addr[self._cur_flow[1]])
        if action not in self.__all_paths[flow]: #if not a valid action, then impose max penalty, we check this here as we dont want to disrupt existin flows and install incorrect flow rules.
            ob = self.get_observation()
            rew = -3 # bad decision , reducing bad decision penalisation. This is because a bad decision doesnt change the network state from congested to less congested, and since we are already penalising for high usage,penatly for bad decision would be double penalisation.
            print("invalid path")
        else:#if correct path is chosen then reward, then check if congestion has reduced.
            print("valid path")
            old_path = self.__flow_paths[flow]
            if old_path != action:
                self.change_path(flow,action,old_path)#flow = ()
                time.sleep(10)#to allow some traffic to pass to make observations
            else:
                print("new path is same as old path")
        
        ob = self.get_observation()
        #print(ob)
        rew = self.get_reward()
        done = False
        if (ob[:8] == np.array([0,0,0,0,0,0,0,0])).all(): # if no more traffic, then end
            done = True
        return ob,rew,done # delete from old switches and install onto new switches.

    def cleanup(self):
        self.net.stop()
        #self.net.start()
        
    def reset(self): # to bring the env to earlier state.
        #self.cleanup()
        self.initialise()

if __name__ == '__main__':
    #setLogLevel('error')
    me = MininetEnv()
    print("!!!!!!!")
    print ("unraveled",me.get_observation())
    print (me.get_reward())

