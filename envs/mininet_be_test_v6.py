#! /usr/bin/python
"""
mininet environment used by rr_gym.
we build a topology
install flows
QoS v1
"""
import os
import sys
import time
from threading import Thread
import socket
import logging
import struct
from mininet.topo import Topo
from mininet.net import Mininet, CLI
from mininet.node import CPULimitedHost,OVSKernelSwitch,Host,RemoteController
from mininet.link import TCLink,Link
from mininet.log import setLogLevel,info
from mininet.cli import CLI

import numpy as np
import random
from itertools import permutations
from itertools import groupby
#from copy import deepcopy
from collections import OrderedDict
from math import modf
from gym_nm_test.envs import utility_lib as ul
from gym_nm_test.envs import flow_setup as fs

class UserTopo(Topo):
    "Simple topology example."
    def __init__(self):
        "Create custom topo."
        # Initialize topology
        Topo.__init__( self )
        
        # Add hosts and switches
        h1 = self.addHost('h1',mac='00:00:00:00:00:01',ip='10.0.0.1')
        h2 = self.addHost('h2',mac='00:00:00:00:00:02',ip='10.0.0.2')
        h3 = self.addHost('h3',mac='00:00:00:00:00:03',ip='10.0.0.3')
        h4 = self.addHost('h4',mac='00:00:00:00:00:04',ip='10.0.0.4')
        h5 = self.addHost('h5',mac='00:00:00:00:00:05',ip='10.0.0.5')
        h6 = self.addHost('h6',mac='00:00:00:00:00:06',ip='10.0.0.6')
      
        sw1 = self.addSwitch('s1')
        sw2 = self.addSwitch('s2')
        sw3 = self.addSwitch('s3')
        sw4 = self.addSwitch('s4')
        sw5 = self.addSwitch('s5')
        
        #
        linkopts = dict(bw=1) #1Mbit
        # Add links
        self.addLink('h1','s1',1,1,cls=TCLink,**linkopts)
        self.addLink('h2','s1',1,2,cls=TCLink,**linkopts)
        self.addLink('h3','s5',1,1,cls=TCLink,bw=1)
        self.addLink('h4','s5',1,2,cls=TCLink,bw=1)
        self.addLink('h5','s2',1,1,cls=TCLink,bw=1)
        self.addLink('h6','s2',1,2,cls=TCLink,bw=1)


        self.addLink('s1','s3',3,1,cls=TCLink,**linkopts)
        self.addLink('s1','s4',4,1,cls=TCLink,**linkopts)
        
        self.addLink('s3','s2',2,3,cls=TCLink,bw=1)
        self.addLink('s4','s2',2,4,cls=TCLink,bw=1)
        
        self.addLink('s5','s3',3,3,cls=TCLink,**linkopts)
        self.addLink('s5','s4',4,3,cls=TCLink,**linkopts)
        
        self.addLink('s3','s4',4,4,cls=TCLink,bw=1)
       
       
class MininetEnv(object):
    def __init__(self): # initialise mininet topo environment
        self.net = Mininet(topo=UserTopo(),controller=lambda a:RemoteController(a,port=6653),ipBase='10.0.0.0/8',link=TCLink)
        self.net.start()
        self.switches = {}
        #update switches dictionary
        for sw in self.net.switches: #sw is ovs switch object
            self.switches[sw.name] = sw # for each access with name
            
        self.port_counter = {1:{1:[0,0],2:[0,0],3:[0,0],4:[0,0]},
                              2:{1:[0,0],2:[0,0],3:[0,0],4:[0,0]},
                              3:{1:[0,0],2:[0,0],3:[0,0],4:[0,0]},
                              4:{1:[0,0],2:[0,0],3:[0,0],4:[0,0]},
                              5:{1:[0,0],2:[0,0],3:[0,0],4:[0,0]}}

        self.links = {(1,3):(3,1),(1,4):(4,1),
                      (2,3):(3,2),(2,4):(4,2),
                      (3,1):(1,3),(3,2):(2,3),(3,3):(5,3),(3,4):(4,4),
                      (4,1):(1,4),(4,2):(2,4),(4,3):(5,4),(4,4):(3,4),
                      (5,3):(3,3),(5,4):(4,3)}
                      
        #mac addr table
        self.mac_addr = OrderedDict([('00:00:00:00:00:01',(1,1)),('00:00:00:00:00:02',(1,2)),('00:00:00:00:00:03',(5,1)),('00:00:00:00:00:04',(5,2)),('00:00:00:00:00:05',(2,1)),('00:00:00:00:00:06',(2,2))]) #
        
        # update arp tables on hosts, 
        ul.update_arp(self.net.hosts)

        self.valid_paths = self.get_all_valid_paths() # all valid paths including last hop.
        self.flow_paths = self.get_flow_paths() # to store current used path
        self.state = [] # observation
        self._time = [0,0] # old time, new timer, port stats calculation
        self.bw = 1048576#1Mbit
        
        self.old_rate = self.bw
        self.cur_rate =self.bw
        self.all_flows = list(self.flow_paths.keys()) # simple list of all flows.to keep a track of flows that we have already trained on
         # to keep a timer on how long we trained on a flow.tick == 100, done = True
        self.qos_flows = {('00:00:00:00:00:01','00:00:00:00:00:05'):500000,('00:00:00:00:00:02','00:00:00:00:00:05'):300000} # all flows that need qos , we are currently using same src,dst to avoid invalid path punishments.
        
        #install default/last resort path for flows
        for flow,path in self.flow_paths.items():
            self.install_flows(flow,path,10) # install default flows to avoid failures at all costs
            
        self.run = 0 # episode number. to be used to select flow 
        self.counter = {} # to store run:change path details
        self.counter[self.run]=0 # to count
        self.sla_miss = {('00:00:00:00:00:01','00:00:00:00:00:05'):0,('00:00:00:00:00:02','00:00:00:00:00:05'):0}
         #book keeping
        self.flow_rates = {('00:00:00:00:00:01','00:00:00:00:00:05'):[0,0],('00:00:00:00:00:02','00:00:00:00:00:05'):[0,0],('00:00:00:00:00:03','00:00:00:00:00:06'):[0,0],('00:00:00:00:00:04','00:00:00:00:00:06'):[0,0],('00:00:00:00:00:05','00:00:00:00:00:01'):[0,0],('00:00:00:00:00:05','00:00:00:00:00:02'):[0,0],('00:00:00:00:00:06','00:00:00:00:00:03'):[0,0],('00:00:00:00:00:06','00:00:00:00:00:04'):[0,0]} #to store previous values of flow rates. bytes/du, gives avg, doesnt give current throughput[duration,bytes]
        self.initialise()
        self.collect_port_stats()
        #CLI(self.net) #--> this seems to block execution of subsequent statements in main()
        #self.cleanup()

    def initialise(self):#resets episode parameters
        self.tick = 0
        n = len(self.switches.keys()) #number of switches
        self.state =  [[-1 for col in range(n)] for row in range(n)] # state info  , -1 for NE links

        for s1,s2 in self.links.items():
            self.state[s1[0]-1][s2[0]-1] = 0
           
        # initialise server on h6 to recieve packets. # test1
        for host in self.net.hosts:# starting hosts on all nodes
            #print(host.name)
            if host.name in ('h6','h5'):
                host.cmd('iperf -s -u -i 1 > server_{}.log &'.format(host.name))
                #host.cmd('iperf -s -u -i 1 > &')
            elif host.name == 'h1':#priority <h1 <--> h5 5 2,
                host.cmd('iperf -c 10.0.0.5 -u -b .5M -t 20000 &') # generate traffic for 20000 secs , 100 ticks * 10 secs sleep
            elif host.name == 'h2':
                host.cmd('iperf -c 10.0.0.5 -u -b .3M -t 20000 &') # generate traffic for 20000 secs , 100 ticks * 10 secs sleep
            #elif host.name in ('h3','h4'):# h3<-->h6 , h4 <--> h6 1,
            #    host.cmd('iperf -c 10.0.0.6 -u -b .2M -t 20000 &') # generate traffic for 20000 secs , 100 ticks * 10 secs sleep
            else:
                toss = random.randint(0,1)
                if toss == 1:
                    hs = ['h3','h4']
                else:
                    hs = [random.choice(['h3','h4'])]
                for host.name in hs:  
                    s = random.randint(0,5000)# delay start
                    b = random.choice([0.1,0.2])# bw to send at
                    d =random.randint(0,(20000-s))# sending duration
                    host.cmd('iperf -c 10.0.0.6 -u -b {}M -t {} &'.format(b,d)) # generate traffic for 20000 secs , 100 ticks * 10 secs sleep
        self.cur_flow = self.get_new_flow() # start training a new flow # current training flow

    def get_new_flow(self): # train each flow for an episode
        i = self.run%len(self.qos_flows)
        s,d = list(self.qos_flows.keys())[i]# s,d are indices
        return (list(self.mac_addr.keys()).index(s),list(self.mac_addr.keys()).index(d)) # indexes.

    ### Path methods            
    def get_all_valid_paths_ori(self):
        valid_paths = {}
        all_paths = ul.get_all_paths(self.links)
        src_sw,dst_sw = 1,4# all srcs are connected to sw 1 and h4 to sw4 
        all_flows = list(permutations(list(self.mac_addr.keys()),2)) #full mac addresss
        for flow in all_flows:
            src_sw = self.mac_addr[flow[0]][0]
            dst_sw = self.mac_addr[flow[1]][0]
            if src_sw == dst_sw : paths = [[self.mac_addr[flow[1]]]]
            else:
                paths = ul.format_path(src_sw,dst_sw,all_paths[(src_sw,dst_sw)],self.links)
                for each_path in paths:
                    each_path.append(self.mac_addr[flow[1]])
            valid_paths[tuple(flow)] = paths
        return valid_paths
        
    def get_all_valid_paths(self):
        return {('00:00:00:00:00:01','00:00:00:00:00:02'): [[(1, 2)]], 
('00:00:00:00:00:01', '00:00:00:00:00:03'): [[(1, 3), (3, 3), (5, 1)], [(1, 3), (3, 4), (4, 3), (5, 1)], [(1, 4), (4, 3), (5, 1)], [(1, 4), (4, 4), (3, 3), (5, 1)]],
('00:00:00:00:00:01', '00:00:00:00:00:04'): [[(1, 3), (3, 3), (5, 2)], [(1, 3), (3, 4), (4, 3), (5, 2)], [(1, 4), (4, 3), (5, 2)], [(1, 4), (4, 4), (3, 3), (5, 2)]], 
('00:00:00:00:00:01', '00:00:00:00:00:05'): [[(1, 3), (3, 2), (2, 1)], [(1, 3), (3, 4), (4, 2), (2, 1)], [(1, 4), (4, 2), (2, 1)], [(1, 4), (4, 4), (3, 2), (2, 1)]],
('00:00:00:00:00:01', '00:00:00:00:00:06'): [[(1, 3), (3, 2), (2, 2)], [(1, 3), (3, 4), (4, 2), (2, 2)], [(1, 4), (4, 2), (2, 2)], [(1, 4), (4, 4), (3, 2), (2, 2)]],
('00:00:00:00:00:02', '00:00:00:00:00:01'): [[(1, 1)]], 
('00:00:00:00:00:02', '00:00:00:00:00:03'): [[(1, 3), (3, 3), (5, 1)], [(1, 3), (3, 4), (4, 3), (5, 1)], [(1, 4), (4, 3), (5, 1)], [(1, 4), (4, 4), (3, 3), (5, 1)]],
('00:00:00:00:00:02', '00:00:00:00:00:04'): [[(1, 3), (3, 3), (5, 2)], [(1, 3), (3, 4), (4, 3), (5, 2)], [(1, 4), (4, 3), (5, 2)], [(1, 4), (4, 4), (3, 3), (5, 2)]],
('00:00:00:00:00:02', '00:00:00:00:00:05'): [[(1, 3), (3, 2), (2, 1)], [(1, 3), (3, 4), (4, 2), (2, 1)], [(1, 4), (4, 2), (2, 1)], [(1, 4), (4, 4), (3, 2), (2, 1)]],
('00:00:00:00:00:02', '00:00:00:00:00:06'): [[(1, 3), (3, 2), (2, 2)], [(1, 3), (3, 4), (4, 2), (2, 2)], [(1, 4), (4, 2), (2, 2)], [(1, 4), (4, 4), (3, 2), (2, 2)]],
('00:00:00:00:00:03', '00:00:00:00:00:01'): [[(5, 3), (3, 1), (1, 1)], [(5, 3), (3, 4), (4, 1), (1, 1)], [(5, 4), (4, 1), (1, 1)], [(5, 4), (4, 4), (3, 1), (1, 1)]],
('00:00:00:00:00:03', '00:00:00:00:00:02'): [[(5, 3), (3, 1), (1, 2)], [(5, 3), (3, 4), (4, 1), (1, 2)], [(5, 4), (4, 1), (1, 2)], [(5, 4), (4, 4), (3, 1), (1, 2)]],
('00:00:00:00:00:03', '00:00:00:00:00:04'): [[(5, 2)]],
('00:00:00:00:00:03', '00:00:00:00:00:05'): [[(5, 3), (3, 2), (2, 1)], [(5, 3), (3, 4), (4, 2), (2, 1)], [(5, 4), (4, 2), (2, 1)], [(5, 4), (4, 4), (3, 2), (2, 1)]],
('00:00:00:00:00:03', '00:00:00:00:00:06'): [[(5, 3), (3, 2), (2, 2)], [(5, 3), (3, 4), (4, 2), (2, 2)], [(5, 4), (4, 2), (2, 2)], [(5, 4), (4, 4), (3, 2), (2, 2)]],
('00:00:00:00:00:04', '00:00:00:00:00:01'): [[(5, 3), (3, 1), (1, 1)], [(5, 3), (3, 4), (4, 1), (1, 1)], [(5, 4), (4, 1), (1, 1)], [(5, 4), (4, 4), (3, 1), (1, 1)]],
('00:00:00:00:00:04', '00:00:00:00:00:02'): [[(5, 3), (3, 1), (1, 2)], [(5, 3), (3, 4), (4, 1), (1, 2)], [(5, 4), (4, 1), (1, 2)], [(5, 4), (4, 4), (3, 1), (1, 2)]],
('00:00:00:00:00:04', '00:00:00:00:00:03'): [[(5, 1)]],
('00:00:00:00:00:04', '00:00:00:00:00:05'): [[(5, 3), (3, 2), (2, 1)], [(5, 3), (3, 4), (4, 2), (2, 1)], [(5, 4), (4, 2), (2, 1)], [(5, 4), (4, 4), (3, 2), (2, 1)]],
('00:00:00:00:00:04', '00:00:00:00:00:06'): [[(5, 3), (3, 2), (2, 2)], [(5, 3), (3, 4), (4, 2), (2, 2)], [(5, 4), (4, 2), (2, 2)], [(5, 4), (4, 4), (3, 2), (2, 2)]],
('00:00:00:00:00:05', '00:00:00:00:00:01'): [[(2, 3), (3, 1), (1, 1)], [(2, 3), (3, 4), (4, 1), (1, 1)], [(2, 4), (4, 1), (1, 1)], [(2, 4), (4, 4), (3, 1), (1, 1)]],
('00:00:00:00:00:05', '00:00:00:00:00:02'): [[(2, 3), (3, 1), (1, 2)], [(2, 3), (3, 4), (4, 1), (1, 2)], [(2, 4), (4, 1), (1, 2)], [(2, 4), (4, 4), (3, 1), (1, 2)]],
('00:00:00:00:00:05', '00:00:00:00:00:03'): [[(2, 3), (3, 3), (5, 1)], [(2, 3), (3, 4), (4, 3), (5, 1)], [(2, 4), (4, 3), (5, 1)], [(2, 4), (4, 4), (3, 3), (5, 1)]],
('00:00:00:00:00:05', '00:00:00:00:00:04'): [[(2, 3), (3, 3), (5, 2)], [(2, 3), (3, 4), (4, 3), (5, 2)], [(2, 4), (4, 3), (5, 2)], [(2, 4), (4, 4), (3, 3), (5, 2)]],
('00:00:00:00:00:05', '00:00:00:00:00:06'): [[(2, 2)]],
('00:00:00:00:00:06', '00:00:00:00:00:01'): [[(2, 3), (3, 1), (1, 1)], [(2, 3), (3, 4), (4, 1), (1, 1)], [(2, 4), (4, 1), (1, 1)], [(2, 4), (4, 4), (3, 1), (1, 1)]],
('00:00:00:00:00:06', '00:00:00:00:00:02'): [[(2, 3), (3, 1), (1, 2)], [(2, 3), (3, 4), (4, 1), (1, 2)], [(2, 4), (4, 1), (1, 2)], [(2, 4), (4, 4), (3, 1), (1, 2)]],
('00:00:00:00:00:06', '00:00:00:00:00:03'): [[(2, 3), (3, 3), (5, 1)], [(2, 3), (3, 4), (4, 3), (5, 1)], [(2, 4), (4, 3), (5, 1)], [(2, 4), (4, 4), (3, 3), (5, 1)]],
('00:00:00:00:00:06', '00:00:00:00:00:04'): [[(2, 3), (3, 3), (5, 2)], [(2, 3), (3, 4), (4, 3), (5, 2)], [(2, 4), (4, 3), (5, 2)], [(2, 4), (4, 4), (3, 3), (5, 2)]],
('00:00:00:00:00:06', '00:00:00:00:00:05'): [[(2, 1)]]}

        
    def get_flow_paths(self):
        flow_paths = {}
        for k in self.valid_paths:
            p_i = np.random.choice(len(self.valid_paths[k]))
            flow_paths[k]= self.valid_paths[k][p_i]
        return flow_paths


    #### installing add and del flow rules
    
    def install_flows(self,flow,path,pri): # based on paths
        for (dp,pr) in path:
            cmd_ = fs.add_flow_cmd(dpid = dp,dl_src = flow[0], dl_dst = flow[1],outport = pr,priority=pri)
            sw = self.switches['s'+str(dp)]
            sw.cmd(cmd_)
            
    def uninstall_flows(self,flow,path,pri):
        for (dp,pr) in path:
            cmd_ = fs.del_flow_cmd(dpid = dp,dl_src = flow[0],dl_dst = flow[1],outport = pr,priority=pri)
            sw = self.switches['s'+str(dp)]
            sw.cmd(cmd_)
   
    def change_path(self,flow,new_path):
        old_path = self.flow_paths[flow]
        if new_path != old_path:
            print("changing path for flow",str(flow) ,str(self.flow_paths[flow]),str(new_path))
            self.uninstall_flows(flow,old_path,20)# remove flow from old_path 
            self.install_flows(flow,new_path,20)## add flows to new_path
            self.flow_paths[flow]=new_path# update self._flow_path to reflect new path
        else:
            print("new path is same as old path")
      
    def collect_port_stats(self):
        #print("in collect port stats module")
        self._time[0] = self._time[1]
        #print ("port counter -->" + str(self.port_counter))
        for dpid in self.port_counter.keys():
            sw = self.switches['s'+str(dpid)]
            sw_pr = fs.port_stats_cmd(sw)
            for port in sw_pr.keys():
                self.port_counter[dpid][port][0] = self.port_counter[dpid][port][1] 
                self.port_counter[dpid][port][1] = sw_pr[port]
        self._time[1] = time.time()
        
    def compute_rate(self,path,flow):# this function computes avg flow rate over duration but doesnt compute delta. this needs to be updated
        path_rate = self.bw # max bw
        for (dpid,pr) in path:# on each link of path measure QoS. If QoS is acheived increase count, else do nothing
            sw = self.switches['s'+str(dpid)] # more precide match critera should be used
            duration,n_bytes = fs.flow_stats_cmd(sw,flow,pr)
            link_rate = (n_bytes/duration)*8 # bps
            if link_rate < path_rate:
                path_rate = link_rate # bottleneck link
        return path_rate

    def update_states(self): # to uodate self.state using port_stats
        #print("port counter-->",self.port_counter)
        #print("time-->",self._time)
        for sw1 in self.port_counter.keys():
            for pr1 in self.port_counter[sw1].keys():
                if (sw1,pr1) not in list(self.mac_addr.values()): # that is not host ports
                    sw2 = self.links[(sw1,pr1)][0]
                    diff = round((self.port_counter[sw1][pr1][1] - self.port_counter[sw1][pr1][0])/(self._time[1]-self._time[0]))
                    load_frac = diff/self.bw
                    if load_frac > 1:load_frac = 1 # setting up a max value
                    self.state[sw1-1][sw2-1]=round(load_frac,3) #continous , is rounding eficient
                    #if diff > self.bw : diff = self.bw
                    #self.state[sw1-1][sw2-1]=(load_frac)*100 # calculating % of load
                else:
                    pass

    ### methods used by gym to observe
    
    def get_observation(self):
        self.collect_port_stats() # 
        self.update_states()
        state = np.ravel(self.state) # unravel into tuple
        #print("raw state-->",state)
        state = state[state != -1] # remove all non-exitent links
        state = np.append(state,self.cur_flow) # add flow,
        state = np.append(state,int(self.cur_rate)) # add cur flow rate
        return state
        
    def get_reward(self,action):
        #calculate chosen path's bottleneck abw and check if QoS can be adhered to.
        # should be based on flow_rate , if flow_rate < QoS then QoS is not met.
        flow = (list(self.mac_addr.keys())[self.cur_flow[0]],list(self.mac_addr.keys())[self.cur_flow[1]])
        path = self.flow_paths[flow]
        rew = 0
        self.cur_rate = self.compute_rate(path,flow)
        req_bw = self.qos_flows[flow]
        print("old_rate-->",self.old_rate,"new_rate-->",self.cur_rate)
        if  self.cur_rate > req_bw: #if sla has met, reward
            rew += 1
            if action == [(0,0)]:
                rew += 1 # if no action was commited then reward further
        elif self.cur_rate < req_bw : 
            rew = -1 
            if action == [(0,0)]:# if no action was performed
                rew += -1 # punish further
            self.sla_miss[flow]+=1
        self.old_rate = int(self.cur_rate) # can be used in next cycle
        print("sla miss-->",self.sla_miss)
        return rew
        
    def get_actions(self):
        va = [[(0,0)]]#no action 
        for f in self.qos_flows:
            va.extend(self.valid_paths[f])
        va.sort()
        return [va for va,_ in groupby(va)]     
        
    def get_BW(self):
        return self.bw
    
    def get_switches(self):
        return len(self.switches.keys())

    def act(self,action): # action is a path for a flow to be installed.
        flow = (list(self.mac_addr.keys())[self.cur_flow[0]],list(self.mac_addr.keys())[self.cur_flow[1]]) # mac addresses
        if action == [(0,0)]: # if no action
            print("no action choosen for flow{}".format(flow))
            
        else:#if correct path is chosen then reward based on how many links in the path have adhered to QoS req
            print("valid path {} for flow{}".format(action,flow))
            old_path = self.flow_paths[flow]
            if old_path != action:
                self.change_path(flow,action)#flow = ()
            else:
                print("new path {} is same as old path {}.nothing to re-configure".format(old_path,action))
        time.sleep(2)#to allow some traffic to pass to make observations
        ob = self.get_observation()# make observation
        rew = self.get_reward(action)
            
        done = False
        self.tick += 1
        if self.tick == 200:# should be same as steps in dqn 
            done=True
            self.tick = 0
            self.run += 1
        return ob,rew,done # delete from old switches and install onto new switches.

    def cleanup(self):
        for host in self.net.hosts:
            host.cmd('sudo pkill iperf &')
        time.sleep(1)

    def reset(self): # to bring the env to earlier state.
        print("resetting...")
        self.cleanup()
        self.initialise()
        
       
if __name__ == '__main__':
    #setLogLevel('error')
    me = MininetEnv()
    print("!!!!!!!")
    time.sleep(1)
    #print("switches-->{}".format(me.switches))
    #
    #print("links-->{}".format(me.links))
    #print("sd-->{}".format(me.sd))
    print("_valid_paths1-->{}".format(me.valid_paths))
    print("flow_paths-->{}".format(me.flow_paths))
    print("get_actions-->{}".format(me.get_actions()))
    print("state-->{}".format(me.state))
    #print("_time-->{}".format(me._time))
    #print("bw-->{}".format(me._bw))
    print("cur_flow-->{}".format(me.cur_flow))
    i = 0
    while i < 20:
        print("_port_counter-->{}".format(me.port_counter))
        print ("unraveled",me.get_observation())
        print("-------")
        print("state-->{}".format(me.state))
        print ("reward",me.get_reward([(0,0)]))
        path = np.random.choice(me.get_actions())
        
        o,r,d = me.act(path)
        if d ==True:
            me.initialise()
        i += 1
    
    #print("_valid_paths2-->{}".format(me._valid_paths))
    #print("actions",me.get_actions())

####notes
#compute_Rate# collects flow stat for a flow on a switch.#we are deleting all flows with priority20 while changing paths. Hence, n_bytes/duration should give the flowrate of current flow.now priority is not accepted by dump-flows, so we match by new port and then choose a flow with priority 20.
        # to distinguish between def flow and new flow, new fow is installed with priority 20. Its a big assumption that there will be only 1 flow matching dst,scr,priority.
