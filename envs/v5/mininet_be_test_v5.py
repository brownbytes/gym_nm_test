#! /usr/bin/python
"""
mininet environment used by rr_gym.
we build a topology
install flows
QoS v1
"""
import os
import sys
import time
from threading import Thread
import socket
import logging
import struct
from mininet.topo import Topo
from mininet.net import Mininet, CLI
from mininet.node import CPULimitedHost,OVSKernelSwitch,Host
from mininet.link import TCLink,Link
from mininet.log import setLogLevel,info
from mininet.cli import CLI
from mininet.node import RemoteController
import numpy as np
import random
from itertools import permutations
from itertools import groupby
from copy import deepcopy
from collections import OrderedDict
from math import modf

class UserTopo(Topo):
    "Simple topology example."
    def __init__(self):
        "Create custom topo."
        # Initialize topology
        Topo.__init__( self )
        
        # Add hosts and switches
        h1 = self.addHost('h1',mac='00:00:00:00:00:01',ip='10.0.0.1')
        h2 = self.addHost('h2',mac='00:00:00:00:00:02',ip='10.0.0.2')
        h3 = self.addHost('h3',mac='00:00:00:00:00:03',ip='10.0.0.3')
        h4 = self.addHost('h4',mac='00:00:00:00:00:04',ip='10.0.0.4')
        
        
        sw1 = self.addSwitch('s1')
        sw2 = self.addSwitch('s2')
        sw3 = self.addSwitch('s3')
        sw4 = self.addSwitch('s4')
        
        #
        linkopts = dict(bw=1) #1Mbit
        # Add links
        self.addLink('h1','s1',1,1,**linkopts)
        self.addLink('h2','s1',1,2,**linkopts)
        self.addLink('h3','s1',1,3,**linkopts)
        self.addLink('h4','s2',1,1,**linkopts)

        self.addLink('s1','s3',4,1,**linkopts)
        self.addLink('s1','s4',5,1,**linkopts)
        
        self.addLink('s2','s3',2,2,**linkopts)
        self.addLink('s2','s4',3,2,**linkopts)
        
        self.addLink('s3','s4',3,3,**linkopts)
       
class MininetEnv(object):
    def __init__(self): # initialise mininet topo environment
        self.net = Mininet(topo=UserTopo(),controller=lambda a:RemoteController(a,port=6653),ipBase='10.0.0.0/8',link=TCLink)
        self.net.start()
        self._switches = {} # self.net.switches is a list of switch objects, this dictionary holds names
        self._port_counter = {1:{1:[0,0],2:[0,0],3:[0,0],4:[0,0],5:[0,0]},
                              2:{1:[0,0],2:[0,0],3:[0,0]},
                              3:{1:[0,0],2:[0,0],3:[0,0]},
                              4:{1:[0,0],2:[0,0],3:[0,0]}}

        self.links = {(1,4):(3,1),(1,5):(4,1),
                      (2,2):(3,2),(2,3):(4,2),
                      (3,1):(1,4),(3,2):(2,2),(3,3):(4,3),
                      (4,1):(1,5),(4,2):(2,3),(4,3):(3,3)}
                      
        #mac addr table
        self._mac_addr = OrderedDict([('00:00:00:00:00:01',(1,1)),('00:00:00:00:00:02',(1,2)),
        ('00:00:00:00:00:03',(1,3)),('00:00:00:00:00:04',(2,1))]) # to access keys using index # since obs is discrete, fl tuple can be (0,0),(0,1) .. so in. hence, an obs can be now sent as an _mac_addr index
        
        
        # update arp tables on hosts, 
        arps = {}
        for i in range(1,len(self.net.hosts)+1):
            host = 'h'+str(i)
            ip = '10.0.0.'+str(i)
            mac = '00:00:00:00:00:0'+str(i)
            arps[host]={'ip':ip,'mac':mac}
        
        for host in self.net.hosts:
            for hh in set(arps.keys())-set([host.name]):# all exclusing the curr host being conf
                host.cmd('arp -s '+ arps[hh]['ip'] + ' ' + arps[hh]['mac'] )
        #update switches dictionary
        for sw in self.net.switches: #sw is ovs switch object
            self._switches[sw.name] = sw # for each access with name
        
        self._valid_paths = self.get_all_valid_paths() # all valid paths including last hop.
        #print(self._valid_paths)
        #print('----')
        self._flow_paths = self.get_flow_paths() # to store current used path
        self._state = [] # observation
        self._lmh = [] # to identify links status as L,M,H [['L','M','H'],['L','M','H']] and calculate rewards

        self._time = [0,0] # old time, new timer, port stats calculation
        self._bw = 1048576#1Mbit
        
        self.old_rate = self._bw # to compare against new rate for issuing rewards
        self.cur_rate = self._bw
        self.all_flows = list(self._flow_paths.keys()) # simple list of all flows.to keep a track of flows that we have already trained on
         # to keep a timer on how long we trained on a flow.tick == 100, done = True
        self.qos_flows = [('00:00:00:00:00:01','00:00:00:00:00:04'),('00:00:00:00:00:02','00:00:00:00:00:04'),('00:00:00:00:00:03','00:00:00:00:00:04')] # all flows that need qos
        
        #install default/last resort path for flows
        for flow,path in self._flow_paths.items():
            self.install_flows(flow,path,10) # install default flows to avoid failures at all costs
            
        self.run = 0 # episode number. to be used to select flow 
        self.counter = {} # to store run:change path details
        self.counter[self.run]=0 # to count
        self.flow_counter = {('00:00:00:00:00:01','00:00:00:00:00:04'):[],('00:00:00:00:00:02','00:00:00:00:00:04'):[],('00:00:00:00:00:03','00:00:00:00:00:04'):[]}
        self.initialise()
        self.collect_port_stats()
        #CLI(self.net) #--> this seems to block execution of subsequent statements in main()
        #self.cleanup()

    def initialise(self):#resets episode parameters
        self.tick = 0
        #_lmh and state dictionaries with defaults 
        n = len(self._switches.keys()) #number of switches
        self._state =  [[-1 for col in range(n)] for row in range(n)] # state info  , -1 for NE links
        self._lmh =  [['NE' for col in range(n)] for row in range(n)] # rewards , NE for nonexistent
        for s1,s2 in self.links.items():
            self._state[s1[0]-1][s2[0]-1] = 0
            self._lmh[s1[0]-1][s2[0]-1] = 'L1'
           
        # initialise server on h6 to recieve packets. # test1
        for host in self.net.hosts:# starting hosts on all nodes
            #print(host.name)
            if host.name =='h4':
                #print("starting server")
                #host.cmd('iperf -s -u > & server_{}.log&'.format(host.name))
                host.cmd('iperf -s -u > &')
            else:
                #print("starting clients")
                host.cmd('iperf -c 10.0.0.4 -u -t 20000 &') # generate traffic for 2500 secs , 100 ticks * 10 secs sleep
        self._cur_flow = self.get_new_flow() # start training a new flow # current training flow

    ### traffic generation traffic_gen.py needs to be run on each host to generate traffic towards a server.
    # we need all sources to generate traffic towards h6 simultaneously. Hence, traffic generation will be a different python script that each host runs on its own
    # the script generates UDP traffic based on pareto distribution, thus reflecting self-similarity.
    
    def get_new_flow(self): # train each flow for an episode
        i = self.run%len(self.qos_flows)
        s,d = self.qos_flows[i]
        return (list(self._mac_addr.keys()).index(s),list(self._mac_addr.keys()).index(d)) # indexes.
                
    ### Path methods
                
    def get_all_valid_paths(self):
        valid_paths = {}
        all_paths = get_all_paths(self.links)
        src_sw,dst_sw = 1,4# all srcs are connected to sw 1 and all servers are connected to switch 6
        all_flows = list(permutations(list(self._mac_addr.keys()),2)) #full mac addresss
        for flow in all_flows:
            src_sw = self._mac_addr[flow[0]][0]
            dst_sw = self._mac_addr[flow[1]][0]
            if src_sw == dst_sw : paths = [[self._mac_addr[flow[1]]]]
            else:
                paths = format_path(src_sw,dst_sw,all_paths[(src_sw,dst_sw)],self.links)
                for each_path in paths:
                    each_path.append(self._mac_addr[flow[1]])
            valid_paths[tuple(flow)] = paths
        return valid_paths
        
    def get_flow_paths(self):
        flow_paths = {}
        for k in self._valid_paths:
            p_i = np.random.choice(len(self._valid_paths[k]))
            flow_paths[k]= self._valid_paths[k][p_i]
        return flow_paths


    #### installing add and del flow rules
    def install_flows(self,flow,path,pri): # based on paths
        for (dp,pr) in path:
            self.add_flow_cmd(dpid = dp,dl_src = flow[0], dl_dst = flow[1],outport = pr,priority=pri)# via s3
            
    def uninstall_flows(self,flow,path,pri):
        for (dp,pr) in path:
            self.del_flow_cmd(dpid = dp,dl_src = flow[0],dl_dst = flow[1],outport = pr,priority=pri)
        
    def add_flow_cmd(self,**kwargs): # match is a dictonary with keywords and actions is a list of actions
        sw = self._switches['s'+str(kwargs['dpid'])]
        cmd_ = 'ovs-ofctl add-flow s{} priority={},dl_src={},dl_dst={},actions=output:{}'.format(kwargs['dpid'],kwargs['priority'],kwargs['dl_src'],kwargs['dl_dst'],kwargs['outport'])
        sw.cmd(cmd_)
            
    def del_flow_cmd(self,**kwargs): 
        sw = self._switches['s'+ str(kwargs['dpid'])]
        cmd_ = 'ovs-ofctl del-flow s{} priority={},dl_src={},dl_dst={},actions=output:{}'.format(kwargs['dpid'],kwargs['priority'],kwargs['dl_src'],kwargs['dl_dst'],kwargs['outport'])
        sw.cmd(cmd_)
        
    def change_path(self,flow,new_path):
        old_path = self._flow_paths[flow]
        if new_path != old_path:#we are deleting and then adding as we dont want the newly added flows to be deleted, instead of old flows.
            # remove flow from old_path
            print("changing path for flow",str(flow) ,str(self._flow_paths[flow]),str(new_path))
            self.uninstall_flows(flow,old_path,20) # we are trying to remove only the high priority flows and leaving default flows as is
            # add flows to new_path
            self.install_flows(flow,new_path,20)#idle timeour 10secs
            # update self._flow_path to reflect new path
            self._flow_paths[flow]=new_path
            #self.counter[self.run] += 1 # to store run:change path details
            
        else:
            print("new path is same as old path")
      
    #### monitor links
        
    def port_stats_cmd(self,dpid):
        #print(dpid)
        sw = self._switches['s'+str(dpid)]
        stats = sw.cmd('ovs-ofctl dump-ports {}'.format(sw)).replace('\r\n',',').split(',')[1:]# omitting the first item which is some text
        #the stats are collected as strings. Hence, string manipulation has been used to extract relevant details.
        p_stats = [stats[x:x+12] for x in range(0,len(stats),12)]
        #print(p_stats)
        for i in range(1,len(p_stats)-1):# omitting port local and an empty list at the end of the list
            port = int(p_stats[i][0][15]) # this is port number
            #print(p_stats[i],p_stats[i][8])
            tx_bytes = p_stats[i][8] # 8th item is tx_bytes
            self._port_counter[dpid][port][0] = self._port_counter[dpid][port][1] # t-1,t#just a counter, not diff
            self._port_counter[dpid][port][1] = int(tx_bytes[7:])*8 #actual tx_bytes value starts from 7th char
            #print(self._port_counter[dpid])

    def collect_port_stats(self):
        #print("in collect port stats module")
        self._time[0] = self._time[1]
        #print ("port counter -->" + str(self._port_counter))
        for sw in self._port_counter.keys():
            self.port_stats_cmd(sw)
        self._time[1] = time.time()
        
    def compute_rate(self,path,flow):
        # collects flow stat for a flow on a switch.#we are deleting all flows with priority20 while changing paths. Hence, n_bytes/duration should give the flowrate of current flow.now priority is not accepted by dump-flows, so we match by new port and then choose a flow with priority 20.
        # to distinguish between def flow and new flow, new fow is installed with priority 20. Its a big assumption that there will be only 1 flow matching dst,scr,priority.
        path_rate = 1000000 # max bw
        for (dpid,pr) in path:# on each link of path measure QoS. If QoS is acheived increase count, else do nothing
            sw = self._switches['s'+str(dpid)] # more precide match critera should be used
            flow_stats = sw.cmd('ovs-ofctl dump-flows {} dl_src={},dl_dst={},out_port={}'.format(sw,flow[0],flow[1],pr)).split('\r\n')[:-1]
            for flow_stat in flow_stats:
                if flow_stat != '' and int(flow_stat.split(',')[5].split("=")[1])==20: # new flow
                    duration = float(flow_stat.split(',')[1].split("=")[1][:-1])
                    n_bytes = int(flow_stat.split(',')[4].split("=")[1])
                elif flow_stat != '' and int(flow_stat.split(',')[5].split("=")[1])==10: # default flow
                    duration = float(flow_stat.split(',')[1].split("=")[1][:-1])
                    n_bytes = int(flow_stat.split(',')[4].split("=")[1])
                link_rate = (n_bytes/duration)*8 # bps
            if link_rate < path_rate:
                path_rate = link_rate # bottleneck link
        return path_rate

    def update_states(self): # to uodate self._state using port_stats
        #print(self._port_counter)
        def get_lmh(lp):
            lp = modf(lp*10)[1] # d*10 --> .97* 10 = 9.7 , modf(9.7) --> (0.6999, 9.0), modf(9.7)[1] --> 9.0
            lmh = {10:'H4',9:'H3',8:'H2',7:'H1',6:'M3',5:'M2',4:'M1',3:'L4',2:'L3',1:'L2',0:'L1'}
            return lmh[lp]
        #print("port counter-->",self._port_counter)
        #print("time-->",self._time)
        for sw1 in self._port_counter.keys():
            for pr1 in self._port_counter[sw1].keys():
                if (sw1,pr1) not in list(self._mac_addr.values()): # that is not host ports
                    sw2 = self.links[(sw1,pr1)][0]
                    diff = round(self._port_counter[sw1][pr1][1] - self._port_counter[sw1][pr1][0])/(self._time[1]-self._time[0])
                    load_frac = diff/self._bw
                    if load_frac > 1:load_frac = 1 # setting up a max value
                    self._state[sw1-1][sw2-1]=round(load_frac,3) #continous , is rounding eficient
                    #if diff > self._bw : diff = self._bw
                    #self._state[sw1-1][sw2-1]=(load_frac)*100 # calculating % of load
                else:
                    pass

    ### methods used by gym to observe
    
    def get_observation(self):
        self.collect_port_stats() # 
        self.update_states()
        state = np.ravel(self._state) # unravel into tuple
        #print("raw state-->",state)
        state = state[state != -1] # remove all non-exitent links
        state = np.append(state,self._cur_flow) # add flow,
        state = np.append(state,int(self.cur_rate)) # add cur flow rate
        return state
        
    def get_reward(self,action):
        #calculate chosen path's bottleneck abw and check if QoS can be adhered to.
        # should be based on flow_rate , if flow_rate < QoS then QoS is not met.
        flow = (list(self._mac_addr.keys())[self._cur_flow[0]],list(self._mac_addr.keys())[self._cur_flow[1]])
        path = self._flow_paths[flow]
        rew = 0
        self.cur_rate = self.compute_rate(path,flow)
        req_bw = 300000
        print("old_rate-->",self.old_rate,"new_rate-->",self.cur_rate)
        if  self.cur_rate > req_bw: #if sla has met, reward
            rew += 1
            if action == [(0,0)]:
                rew += 1 # if no action was commited then reward further
        elif self.cur_rate < req_bw : #if QoSt < rate : penalise (-1) irrespectiv of rate > old_Rate or old_rate < 260000
            rew = -1 
            if action == [(0,0)]:# if no action was performed
                rew += -1 # punish further
        self.flow_counter[flow].append(self.cur_rate)
        self.old_rate = int(self.cur_rate) # can be used in next cycle
        return rew
        
    def get_actions(self):
        va = [[(0,0)]]#no action 
        for f in self.qos_flows:
            va.extend(self._valid_paths[f])
        va.sort()
        return [va for va,_ in groupby(va)]     
        
    def get_BW(self):
        return self._bw
    
    def get_switches(self):
        return len(self._switches.keys())

    def act(self,action): # action is a path for a flow to be installed.
        flow = (list(self._mac_addr.keys())[self._cur_flow[0]],list(self._mac_addr.keys())[self._cur_flow[1]]) # mac addresses
        if action == [(0,0)]: # if no action
            print("no action choosen for flow{}".format(flow))
            
        else:#if correct path is chosen then reward based on how many links in the path have adhered to QoS req
            print("valid path {} for flow{}".format(action,flow))
            old_path = self._flow_paths[flow]
            if old_path != action:
                self.change_path(flow,action)#flow = ()
            else:
                print("new path {} is same as old path {}.nothing to re-configure".format(old_path,action))
        time.sleep(2)#to allow some traffic to pass to make observations
        ob = self.get_observation()# make observation
        rew = self.get_reward(action)
            
        done = False
        self.tick += 1
        if self.tick == 100:# should be same as steps in dqn 
            done=True
            self.tick = 0
            self.run += 1
        return ob,rew,done # delete from old switches and install onto new switches.

    def cleanup(self):
        for host in self.net.hosts:
            host.cmd('sudo pkill iperf &')
        time.sleep(1)

    def reset(self): # to bring the env to earlier state.
        print("resetting...")
        self.cleanup()
        self.initialise()
        
#### DFS

def commence_dfs(s,d,topo):
    return dfs(topo,s,d,list(),list())
    
def dfs(G,cur_node,dst,visited,all_paths): # dfs using recursion
  visited.append(cur_node) # mark src as visited
  if cur_node == dst: # base condition
    all_paths.append(deepcopy(visited))## since path gets modified, the modification also seems to be affecting all_paths, hence a deepcopy
  else:
    for nei in G[cur_node]:
      if nei not in visited:
        dfs(G,nei,dst,visited,all_paths) # same variable path is being updated
  visited.pop() #visited.remove(cur_node)
  return all_paths

def get_all_paths(links):
    all_sd = list(permutations(range(1,5),2))
    paths = {}
    for (s,d) in all_sd:
        paths[(s,d)]=[]
    topo = {1:[],2:[],3:[],4:[]}
    for n1,n2 in links.items():
        topo[n1[0]].append(n2[0])
    for s,d in all_sd:
        c = commence_dfs(s,d,topo)
        paths[(s,d)]=c
    #acp = format_path(1,6,paths,links)
    return paths

def format_path(s,d,paths,links):
    completed_path = []
    all_completed_paths =[]
    for path in paths:
        for i in range(0,len(path)-1):
            for n1,n2 in links.items():
                if n1[0] == path[i] and n2[0] == path[i+1]:
                    completed_path.append(n1)
                else:
                    continue
        all_completed_paths.append(completed_path)
        completed_path = []
    return all_completed_paths
        
if __name__ == '__main__':
    #setLogLevel('error')
    me = MininetEnv()
    print("!!!!!!!")
    time.sleep(1)
    #print("_switches-->{}".format(me._switches))
    #
    #print("links-->{}".format(me.links))
    #print("sd-->{}".format(me.sd))
    print("_valid_paths1-->{}".format(me._valid_paths))
    print("flow_paths-->{}".format(me._flow_paths))
    print("get_actions-->{}".format(me.get_actions()))
    #print("state-->{}".format(me._state))
    #print("lmh-->{}".format(me._lmh))
    #print("_time-->{}".format(me._time))
    #print("bw-->{}".format(me._bw))
    #print("cur_flow-->{}".format(me._cur_flow))
    i = 0
    while i < 5:
        print("_port_counter-->{}".format(me._port_counter))
        print ("unraveled",me.get_observation())
        print("-------")
        print("state-->{}".format(me._state))
        print("lmh-->{}".format(me._lmh))
        print ("reward",me.get_reward([(0,0)]))
        path = np.random.choice(me.get_actions())
        
        o,r,d = me.act(path)
        if d ==True:
            me.initialise()
        i += 1
    
    #print("_valid_paths2-->{}".format(me._valid_paths))
    #print("actions",me.get_actions())


